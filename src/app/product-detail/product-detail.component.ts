
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from '../shared/product.service';
import { Product } from '../shared/model/product';
import { Location } from '../shared/model/location';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
  product: Product = {};
  productId = '';
  location: Location = {};
  overlays: any[];
  loading = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    public productService: ProductService,
    private ref: ChangeDetectorRef,
  ) { }

  ngOnInit() {
    this.productId = this.activatedRoute.snapshot.params['id'];

    this.generateData(this.productId);
  }

  public async generateData(id: string) {
    this.product = await this.productService.getProductById(id);

    this.addressToCoordinates();
  }

  addressToCoordinates() {
    this.loading = true;
    this.productService.geocodeAddress(this.product.address, 15)
    .subscribe((location: any) => {
        this.location = location;
        this.loading = false;
        this.ref.detectChanges();  
      }      
    );     
  }
}
