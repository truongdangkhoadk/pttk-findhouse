import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ProductService } from '../shared/product.service';
import { Product } from '../shared/model/product';
import {SelectItem} from 'primeng/api';
import { Router } from '@angular/router';
@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  products: any;
  selectedProduct: Product;
  displayDialog: boolean;
  sortKey: string;
  sortOptions: SelectItem[];
  sortOrder: number;
  sortField: string;

  sortDistrict: SelectItem[];

  constructor(
    private productService: ProductService,
    public router: Router,
    private ref: ChangeDetectorRef,
  ) { }

  ngOnInit(): void {
    this.generateData();
    
    this.sortOptions = [
      {label: 'Giá Cao đến Thấp', value: '!price'},
      {label: 'Giá Thấp đến Cao', value: 'price'}
    ];
    this.sortDistrict = [
      {label: 'Quận 1', value: 'Quận 1'},
      {label: 'Quận 2', value: 'Quận 2'},
      {label: 'Quận 5', value: 'Quận 5'},
      {label: 'Quận 7', value: 'Quận 7'},
      {label: 'Quận 8', value: 'Quận 4'},
      {label: 'Quận 11', value: 'Quận 11'},
      {label: 'Quận Tân Bình', value: 'Quận Tân Bình'},
      {label: 'Quận Bình Tân', value: 'Quận Bình Tân'},
      {label: 'Quận Gò Vấp', value: 'Quận Gò Vấp'},
    ];
  }

  public async generateData() {
    const results = await this.productService.getProductList();
    if (results) {
      this.products = results.data;
    } 
  }

  selectProduct(event: Event, product: Product) {
    this.selectedProduct = product;
    this.router.navigateByUrl(`home/product-detail/${this.selectedProduct.id}`)
    // this.displayDialog = true;
    event.preventDefault();
  }

  onSortChange(event) {
    let value = event.value;

    if (value.indexOf('!') === 0) {
        this.sortOrder = -1;
        this.sortField = value.substring(1, value.length);
    }
    else {
        this.sortOrder = 1;
        this.sortField = value;
    }
  }

  onSortDistrict(event) {
    const district = event.value;
    this.filterByDistrict(district);
  }

  public async filterByDistrict(district: string) {
    let data: any = [];

    if (!district) {
      this.generateData();
    } else {
      const results = await this.productService.getProductByDistrict(district);

      if (results) {
        results.forEach(x => {
          data.push(x);
        })

        this.products = data;
      }
    }
  }
}
