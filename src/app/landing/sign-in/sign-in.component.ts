import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder
  ) { }

  loginForm: FormGroup;
  submitted = false;

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]],
      fcaptcha: [true, Validators.required]
    });
  }
  get f() { return this.loginForm.controls; }

  onSubmit() {  
    this.submitted = true;
    console.log(this.loginForm.controls);
  }

  resolved(event: any) {
    this.f.fcaptcha.setValue(false);
  }
}
