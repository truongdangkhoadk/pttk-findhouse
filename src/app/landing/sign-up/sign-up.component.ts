import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder
  ) { }
  signupForm: FormGroup;
  submitted = false;

  ngOnInit(): void {
    this.signupForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]],
      confirmpassword: ['', [Validators.required]]
    }, { validator: this.checkPasswords });
  }

  get f() {
    return this.signupForm.controls;
  }

  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    const pass = group.get('password').value;
    const confirmPass = group.get('confirmpassword').value;

    return pass === confirmPass ? null : this.f.confirmpassword.setErrors({'notSame': true})     
  }

  onSubmit() {  
    this.submitted = true;
    this.checkPasswords(this.signupForm);
  }
}
