export interface Product {
    id?: number;
    title?: string;
    price?: string;
    intro?: string;
    img?: string;
    acreage?: string;
    area?: string;
    address?: string;
    rating?: string;
}