export interface Location {
    lat?: string;
    lng?: string;
    zoom?: number
}