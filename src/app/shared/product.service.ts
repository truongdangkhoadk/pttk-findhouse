import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {} from "googlemaps"
import { MapsAPILoader } from '@agm/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { of } from 'rxjs/observable/of';
import { filter, catchError, tap, map, switchMap, last } from 'rxjs/operators';
import { fromPromise } from 'rxjs/observable/fromPromise';
import { Location } from '../shared/model/location';

declare var google: any;

@Injectable()
export class ProductService {
    private geocoder: any;

    constructor(
        private http: HttpClient,
        private mapLoader: MapsAPILoader
    ) { }

    public async getProductList() {
        const data = await this.http.get<any>('/assets/mock-data/product.json').toPromise();
        return data;
    }

    public async getProductById(id: string) {
        const products = await this.getProductList();
        const data = products.data.find(x => x.id === +id);
        console.log(data);
        return data;
    }

    public async getProductByDistrict(district: string) {
        const products = await this.getProductList();
        const data = products.data.filter(x => x.acreage === district);
        console.log(data);
        return data;
    }



    private initGeocoder() {
        console.log('Init geocoder!');
        this.geocoder = new google.maps.Geocoder();
    }

    private waitForMapsToLoad(): Observable<boolean> {
        if(!this.geocoder) {
            return fromPromise(this.mapLoader.load())
            .pipe(
            tap(() => this.initGeocoder()),
            map(() => true)
            );
        }
        return of(true);
    }

    public geocodeAddress(location: string, zoomValue: number): Observable<Location> {
        console.log('Start geocoding!');
        this.geocoder = new google.maps.Geocoder();
        // this.waitForMapsToLoad().pipe(
        //     // filter(loaded => loaded),
        //     switchMap(() =>
        return  new Observable(observer => {
                this.geocoder.geocode({ 'address': location }, (results, status) => {
                    if (status == google.maps.GeocoderStatus.OK) {
                        console.log('Geocoding complete!');

                        const data = {
                            lat: results[0].geometry.location.lat(),
                            lng: results[0].geometry.location.lng(),
                            zoom: zoomValue
                        }

                        observer.next(data);
                    }
                    // else {
                    //     console.log('Error - ', results, ' & Status - ', status);
                    //     observer.next({ lat: 0, lng: 0 });
                    // }
                    observer.complete();
                });
             }
            //))
         )
    }
}