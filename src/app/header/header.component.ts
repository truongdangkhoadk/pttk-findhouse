import { Component, OnInit } from '@angular/core';
import {MenuItem} from 'primeng/api';
import { Router } from '@angular/router'

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  items: MenuItem[];
  activeItem: MenuItem;

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
    this.items = [
      {label: 'Trang chủ', icon: 'pi pi-fw pi-home'},
      {label: 'Phòng trọ', icon: 'pi pi-fw pi-calendar'},
      {label: 'Căn hộ', icon: 'pi pi-fw pi-pencil'},
      {label: 'Hướng dẫn', icon: 'pi pi-fw pi-pencil'},
    ];
    this.activeItem = this.items[0];
  }

  switch(tab: any) {
    console.log(tab);
  }

  redirectLogin() {
    this.router.navigateByUrl("signin");
  }
}
